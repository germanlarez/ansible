vpc_cidr_block      = "10.0.0.0/16"
subnet_cidr_block   = "10.0.10.0/24"
avail_zone          = "us-east-1a"
env_prefix          = "dev"
my_ip               = ["190.82.204.67/32"]
instance_type       = "t3.medium"
public_key_location = "~/.ssh/terraform-key.pub"
private_key_location = "~/.ssh/terraform-key"
