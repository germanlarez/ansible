resource "aws_instance" "ansible-server" {
  ami                         = data.aws_ami.latest-amazon-linux-ami-ansible.id
  instance_type               = var.instance_type
  subnet_id                   = aws_subnet.myapp-priv-subnet-1.id
  vpc_security_group_ids      = [aws_security_group.myapp-sg.id]
  availability_zone           = var.avail_zone
  associate_public_ip_address = true
  key_name                    = aws_key_pair.ssh-key.key_name
  user_data                   = file("ansible-entry-script.sh")

  connection {
    type = "ssh"
    host = self.public_ip
    user = "ubuntu"
    private_key = file(var.private_key_location)
    timeout = "5m"
  }

  provisioner "file" {
    source = "~/.ssh/terraform-key"
    destination = "/home/ubuntu/.ssh/terraform-key"  
    }
  
  provisioner "remote-exec" {
    inline = [ 
      "chmod 400 /home/ubuntu/.ssh/terraform-key"
     ]
  }

  tags = {
    Environment = "${var.env_prefix}"
    Name        = "ansible-${var.env_prefix}"
  }
}