resource "aws_key_pair" "ssh-key" {
  key_name   = "${var.env_prefix}-ansible-server-managed"
  public_key = file(var.public_key_location)
}