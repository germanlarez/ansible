output "aws_ami_id_ansible" {
  value = data.aws_ami.latest-amazon-linux-ami-ansible.id
}

output "aws_ami_id_ansible_managed" {
  value = data.aws_ami.latest-amazon-linux-ami-managed.id
}

output "ec2_public_ip_ansible" {
  value = aws_instance.ansible-server.public_ip
}

output "ec2_private_ip_managed" {
  value = aws_instance.ansible-managed.private_ip
}

output "ec2_private_ip_managed_2" {
  value = aws_instance.ansible-managed-2.private_ip
}

output "ec2_public_ip_managed" {
  value = aws_instance.ansible-managed.public_ip
}