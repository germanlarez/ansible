resource "aws_instance" "ansible-managed-2" {
  ami                         = data.aws_ami.latest-amazon-linux-ami-managed.id
  instance_type               = var.instance_type
  subnet_id                   = aws_subnet.myapp-priv-subnet-1.id
  vpc_security_group_ids      = [aws_security_group.myapp-sg.id]
  availability_zone           = var.avail_zone
  associate_public_ip_address = true
  key_name                    = aws_key_pair.ssh-key.key_name
  user_data                   = file("managed-entry-script.sh")
  tags = {
    Environment = "${var.env_prefix}"
    Name        = "ansible-managed-2-${var.env_prefix}"
  }
}